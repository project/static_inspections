# RAFT Coder

Configures inspections for drupal projects.

## INTRODUCTION

This module installs PHP CodeSniffer and PHP Mess Detector. 

The module also installs inspection profiles from Drupal Coder and PHPCS
Security Audit, and provides a ready-to-go XML file that runs all 3
inspection profiles at once. These profiles are currently unmodified, but in
the future the module can serve as a way to distribute standard inspection
profiles to all our developer IDEs and help us develop a consensus of how we
want our code to be structured.

The PHP Mess detector profile is drawn from Drupal Template Whisperer
(http://cgit.drupalcode.org/template_whisperer/tree/phpmd.xml) and is likewise
assumed to be a starting point for discussion and distribution of a common set
of practices.

## REQUIREMENTS

This module requires the following modules:

* Coder (https://www.drupal.org/project/coder)
* phpcs-security-audit (https://github.com/FloeDesignTechnologies/phpcs-security-audit)
* phpmd (https://phpmd.org/)

If updating this module, be aware that only requires are inherited - require-dev modules
will not be passed in to the parent.

## INSTALLATION

In composer.json:

1) add `"url": "git@bitbucket.org:raftdev/raft-coder"` as a `vcs` code repository
2) add `"raftdev/raft-coder": "~1.0"` to `require-dev`

Then run `composer update`

## CONFIGURATION

### PhpStorm Settings

1) Ensure a PHP executable is configured for the projecy
2) Add a configuration to Languages + Frameworks > PHP > Code Sniffer,
 selecting <project>/vendor/bin/phpcs
3) Add a configuration to Languages + Frameworks > PHP > Mess Detector,
 selecting <project>/vendor/bin/phpmd
4) Select a profile in Editor > Inspections > PHP > PHP Code Sniffer validations,
 choose custom and <project>/vendor/raftdev/raft-coder/phpcs.xml
4) Select a profile in Editor > Inspections > PHP > PHP Mess Dectector validations,
 choose custom and <project>/vendor/raftdev/raft-coder/phpmd.xml
